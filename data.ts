import { Ability, Creep, Hero, RendererSDK, Roshan, RoshanSpawner, SpiritBear, Unit } from "wrapper/Imports"

export class BARSData {

	public static Creeps: Creep[] = []
	public static Roshan: Nullable<Roshan>
	public static Units: (Hero | SpiritBear)[] = []
	public static DrawDamageCache = new Map<Unit, [number]>()

	public static CacheUnits: (Hero | SpiritBear)[] = []
	public static RoshanSpawner: Nullable<RoshanSpawner>

	public static IgnorCaster: string[] = [
		"npc_dota_hero_templar_assassin",
		"npc_dota_hero_batrider",
		"npc_dota_hero_faceless_void",
		"npc_dota_hero_clinkz",
		"npc_dota_hero_lone_druid",
		"npc_dota_hero_naga_siren",
		"npc_dota_hero_chen",
		"npc_dota_hero_keeper_of_the_light",
	]

	public static IgnoreAbility: string[] = [
		"techies_land_mines",
		"techies_remote_mines",
		"techies_stasis_trap",
		"techies_remote_mines_self_detonate",
		"techies_minefield_sign",
	]

	/** use only draw event */
	public static get ScreenSize() {
		return RendererSDK.WindowSize
	}

	public static ShowType(selector: number, num: number, max_num: number) {
		switch (selector) {
			case 0: return `${Math.floor(num)}`
			case 1: return `${Math.floor(num)}/${Math.floor(max_num)}`
			case 2: return `${Math.floor(Math.floor(num) / Math.floor(max_num) * 100)}%`
			case 3: return `${Math.floor(num)} | ${Math.floor(Math.floor(num) / Math.floor(max_num) * 100)}%`
			case 4: return `${Math.floor(num)}/${Math.floor(max_num)} | ${Math.floor(Math.floor(num) / Math.floor(max_num) * 100)}%`
		}
		return ""
	}

	public static Dispose() {
		this.Units = []
		this.Creeps = []
		this.CacheUnits = []
		this.Roshan = undefined
		this.DrawDamageCache.clear()
		this.RoshanSpawner = undefined
	}

	public static FilterAbility = (abil: Ability) => !abil.IsHidden
		&& !abil.Name.includes("_empty")
		&& !abil.Name.includes("special_")
		&& !BARSData.AbilitySpecialHidden.includes(abil.Name)

	private static readonly AbilitySpecialHidden: string[] = [
		"high_five",
		"seasonal_ti9_banner",
		"morphling_morph_agi",
		"morphling_morph_str",
		"invoker_empty1",
		"invoker_empty2",
		"generic_hidden",
		"rubick_hidden1",
		"rubick_hidden2",
		"rubick_hidden3",
		"seasonal_ti10_",
	]
}
