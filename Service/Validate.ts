import { GameRules, Hero, LocalPlayer, npc_dota_hero_meepo, SpiritBear } from "wrapper/Imports"
import { BarsState } from "../menu"

export class ServiceValidate {

	public static get IsInGame() {
		return BarsState.value
			&& GameRules !== undefined
			&& GameRules.IsInGame
			&& LocalPlayer !== undefined
			&& LocalPlayer.Hero !== undefined
			&& !LocalPlayer.IsSpectator
	}

	public static IsRealHero(x: Hero | SpiritBear) {
		return x !== undefined && x.IsValid
			&& (x instanceof Hero)
			&& !x.IsIllusion && x.IsEnemy()
			&& !(x instanceof npc_dota_hero_meepo && x.IsClone)
	}

	public static IsHeroSummoned(x: Hero | SpiritBear) {
		return x !== undefined && x.IsValid
			&& x.IsEnemy()
			&& (x instanceof Hero || x instanceof SpiritBear)
			&& (!x.IsIllusion || x.IsTempestDouble)
	}
}
