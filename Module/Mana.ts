
import { Color, RendererSDK, Unit, Vector2 } from "wrapper/Imports"
import { PathX, RectangleDraw, RectangleX, UnitBarsX } from "X-Core/Imports"
import { BARSData } from "../data"
import { ManaBorderColor, ManaColorBar, ManaPositionVector, ManaSizeVectorX, ManaSizeVectorY, ManaTextState, NumberModeMP } from "../menu"

export let ManaModule = (unit: Unit) => {

	const bars = new UnitBarsX(unit)
	const MPPosition = bars.HealthBarPosition
	if (MPPosition.IsZero())
		return

	const manaBarSize = bars.HealthBarSize
	const text = BARSData.ShowType(NumberModeMP.selected_id, unit.Mana, unit.MaxMana)
	const vector = new Vector2(manaBarSize.x, Math.floor(manaBarSize.y * 0.53))
		.AddForThis(new Vector2(ManaSizeVectorX.value, ManaSizeVectorY.value))
	const location = MPPosition.AddForThis(new Vector2(0, manaBarSize.y + 1)).AddForThis(ManaPositionVector.Vector)

	if (vector.y <= 0)
		return

	const Rectangle = new RectangleX(location, vector.Clone()).AddSizeVector(1.5)
	const Rectangle2 = new RectangleX(location, new Vector2(vector.x * Math.max((unit.ManaPercent / 100), 0.001), vector.y))

	RectangleDraw.FilledRect(Rectangle, ManaBorderColor.selected_color)
	RectangleDraw.Image(PathX.Images.topbar_mana, Rectangle2, ManaColorBar.selected_color)

	if (ManaSizeVectorY.value <= 4 || !ManaTextState.value)
		return

	const vec = new Vector2(Rectangle.X, Rectangle.Y + (Rectangle.Height / 6))
		.AddScalarX((Rectangle.Width - RendererSDK.GetTextSize(text, "Calibri", Rectangle.Height).x) / 2)

	RendererSDK.Text(text, vec, Color.White, "Calibri", Rectangle.Height)
}
