import { Color, RendererSDK, Unit, Vector2 } from "wrapper/Imports"
import { AdditionalOverlayPosition, AdditionalOverlaySize, BarsBlindState } from "../menu"
import { UnitBarsX } from "../X-Core/Imports"

const ColorBlind = new Color(86, 156, 184)
export let HealthDamageModule = (DamageCache: Map<Unit, [number]>) => {
	DamageCache.forEach((dmg, hero) => {

		if (!hero.IsAlive || !hero.IsVisible)
			return

		const Bars = new UnitBarsX(hero)
		const healthBarPosition = Bars.HealthBarPosition
		if (healthBarPosition.IsZero())
			return

		const healthBarSize = Bars.HealthBarSize
		const x2 = Math.min(dmg[0], hero.HP) / hero.MaxHP
		const vector = healthBarPosition
			.AddForThis(new Vector2(0, healthBarSize.y * 0.7))
			.AddForThis(AdditionalOverlayPosition.Vector)

		const vector2 = healthBarSize
			.MultiplyForThis(new Vector2(x2, 0.4))
			.AddScalarY(AdditionalOverlaySize.value)

		const colorDmg = BarsBlindState.value
			? ((dmg[0] >= hero.HP)
				? ColorBlind.SetColor(102, 237, 237)
				: ColorBlind)
			: ((dmg[0] >= hero.HP)
				? Color.Green
				: Color.Green.SetColor(108, 140, 53))

		RendererSDK.FilledRect(vector, vector2, colorDmg)
	})
}
