import { Color, RendererSDK, Roshan, Vector2 } from "wrapper/Imports"
import { PathX, RectangleDraw, RectangleX, UnitBarsX } from "X-Core/Imports"
import { BARSData } from "../data"
import { BarsBlindState, NumberModeHPRoshan, RoshanHColorBar, RoshanHPositionVector, RoshanHSizeVector } from "../menu"

const BlackOpacity = Color.Black.SetA(185)

export let RoshanModule = () => {

	if (Roshan.HP === 0 || BARSData.RoshanSpawner === undefined)
		return

	const Position = (BARSData.Roshan !== undefined && BARSData.Roshan.IsVisible)
		? BARSData.Roshan.Position.AddScalarZ(133)
		: BARSData.RoshanSpawner.Position

	const Bars = new UnitBarsX(Position)
	const healthBarPosition = Bars.HealthBarPosition

	if (healthBarPosition.IsZero())
		return

	const healthPercent = Math.floor(Roshan.HP / Roshan.MaxHP * 100)
	const manaBarSize = Bars.HealthBarSize
	const RoshBarsText = BARSData.ShowType(NumberModeHPRoshan.selected_id, Roshan.HP, Roshan.MaxHP)
	const vector = new Vector2(manaBarSize.x, Math.floor(manaBarSize.y * 0.53)).AddForThis(RoshanHSizeVector.Vector)
	const location = healthBarPosition.AddForThis(new Vector2(0, manaBarSize.y + 1)).AddForThis(RoshanHPositionVector.Vector)
	if (vector.y <= 0)
		return

	const rec = new RectangleX(location, new Vector2(vector.x, vector.y))
	const rec2 = new RectangleX(location, new Vector2(vector.x, vector.y))

	rec.RectangleXVector(location, vector.x, vector.y).AddSizeVector(1.5)
	rec2.RectangleXVector(location, vector.x * ((healthPercent / 100) <= 0.01 ? 0.001 : (healthPercent / 100)), vector.y)

	RectangleDraw.FilledRect(rec, BlackOpacity)
	RectangleDraw.Image((!BarsBlindState.value ? PathX.Images.topbar_health : PathX.Images.topbar_health_blind), rec2, RoshanHColorBar.selected_color)

	if (RoshanHSizeVector.Vector.y === 0)
		return

	const vector2 = new Vector2(rec.X, rec.Y + rec.Height / 6)
		.AddScalarX((rec.Width - RendererSDK.GetTextSize(RoshBarsText, "Calibri", rec.Height).x) / 2)

	RendererSDK.Text(RoshBarsText, vector2, Color.White, "Calibri", rec.Height)
	RendererSDK.Image(PathX.Images.roshan_timer_roshan, new Vector2(rec.X - 42, rec.Y - 10), -1, new Vector2(44, 44))
}
