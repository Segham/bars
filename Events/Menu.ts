import { BARSData } from "../data"
import { DamageBarsState } from "../menu"

DamageBarsState.OnDeactivate(() =>
	BARSData.DrawDamageCache.clear())
