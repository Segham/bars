import { DOTAGameUIState_t, EventsSDK, GameState, Hero, SpiritBear } from "wrapper/Imports"
import { BARSData } from "../data"
import { DamageBarsState, HealthState, ManaBarState, RoshanHealthState } from "../menu"
import { HealthDamageModule, HealthModule, ManaModule, RoshanModule } from "../Module/index"
import { ServiceValidate } from "../Service/Validate"

EventsSDK.on("Draw", () => {
	if (!ServiceValidate.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return

	if (RoshanHealthState.value)
		RoshanModule()

	const filter = BARSData.CacheUnits.filter(x => x.IsAlive && x.IsVisible)

	filter.forEach(unit => {

		if (!(unit instanceof Hero || unit instanceof SpiritBear))
			return

		if (ManaBarState.value)
			ManaModule(unit)

		if (HealthState.value)
			HealthModule(unit)
	})

	if (DamageBarsState.value)
		HealthDamageModule(BARSData.DrawDamageCache)
})
