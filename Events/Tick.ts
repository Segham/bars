import { DAMAGE_TYPES, EventsSDK, Hero, item_tpscroll, LocalPlayer, SpiritBear, storm_spirit_overload } from "wrapper/Imports"
import { DamageX } from "X-Core/Imports"
import { BARSData } from "../data"
import { BarsStateSummoned, DamageBarsState } from "../menu"
import { ServiceValidate } from "../Service/Validate"

EventsSDK.on("Tick", () => {
	if (!ServiceValidate.IsInGame)
		return

	BARSData.CacheUnits = BARSData.Units.filter(x => !BarsStateSummoned.value
		? ServiceValidate.IsRealHero(x)
		: ServiceValidate.IsHeroSummoned(x),
	)

	if (!DamageBarsState.value)
		return

	const unit = LocalPlayer!.Hero! // TODO Meepo & unit IsControllable

	const abilities = unit.Spells.filter((abil, i) => {
		return abil !== undefined && i < 6
			&& !abil.IsHidden
			&& abil.CanBeCasted()
			&& BARSData.FilterAbility(abil)
			&& BARSData.IgnoreAbility.indexOf(abil.Name) === -1
			&& BARSData.IgnorCaster.indexOf((abil.Owner as Hero).Name) === -1
			&& (!abil.IsPassive || abil instanceof storm_spirit_overload)
			&& abil.DamageType !== DAMAGE_TYPES.DAMAGE_TYPE_NONE
	}).concat(unit.Items.filter(x => !(x instanceof item_tpscroll)
		&& x.CanBeCasted()
		&& !x.IsPassive
		&& x.DamageType !== DAMAGE_TYPES.DAMAGE_TYPE_NONE,
	))

	BARSData.CacheUnits.forEach(target => {

		const cache_damage = BARSData.DrawDamageCache.get(target)

		if (abilities.length === 0 && BARSData.DrawDamageCache.has(target)) {
			BARSData.DrawDamageCache.delete(target)
			return
		}

		if (!target.IsVisible || !target.IsAlive)
			return

		const TotalDamage = abilities.map(ability => new DamageX(
			ability!,
			target,
			BARSData.CacheUnits as (SpiritBear & Hero)[],
			BARSData.Creeps,
		).GetDamage).reduce((prev, curr) => prev + curr, 0)

		if (cache_damage === undefined) {
			BARSData.DrawDamageCache.set(target, [TotalDamage])
			return
		}

		cache_damage[0] = TotalDamage
	})
})
