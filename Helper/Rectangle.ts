import { Color, Rectangle, RendererSDK, Vector2 } from "wrapper/Imports"

export class RectangleX extends Rectangle {

	public static OutlinedRect(rect: RectangleX, color?: Color) {
		RendererSDK.OutlinedRect(rect.pos1, rect.pos2, 1, color)
	}

	public static FilledRect(rect: RectangleX, color?: Color) {
		RendererSDK.FilledRect(rect.pos1, rect.pos2, color)
	}

	public static Image(path: string, rect: RectangleX, color?: Color, round: number = -1) {
		RendererSDK.Image(path, rect.pos1, round, rect.pos2, color)
	}
	constructor(public pos1: Vector2, public pos2: Vector2) {
		super(pos1, pos2)
	}

	public AddSizeVector(size: number) {
		this.pos1.SubtractScalarForThis(size)
		this.pos2.AddScalarForThis(size).MultiplyScalar(2)
		return this
	}

	public Add(location: Vector2) {
		return new RectangleX(new Vector2(this.pos1.x + location.x, this.pos1.y + location.y), this.pos2)
	}
}
